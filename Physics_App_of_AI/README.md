# README.md


## Galaxy Identification Project

Welcome to the Galaxy Identification Project! This project aims to classify and analyze images of galaxies based on their shapes using machine learning techniques. This README will guide you through the project setup, structure, and how to run the different tasks.

## Table of Contents

1. Introduction
2. Setup and Installation
3. Running the Project
    - Training models
    - Evaluating Models
4. Tasks and Exercises

## Introduction

In this project, I examine and classify images of galaxies based on their shapes. The dataset comes from a Kaggle Challenge which uses crowdsourcing, with non-experts categorizing galaxy images. Each image is assigned a floating-point number between zero and one, indicating the fraction of participants who assigned the image to a specific class.

This project includes three main tasks:
1. Classification
2. Regression
3. Advanced Regression (Exercise 3)

## Setup and Installation

### Prerequisites

Make sure you have Python 3.10 or higher installed on your system.

### Create and Activate a Virtual Environment

It’s recommended to use a virtual environment to manage dependencies:

```bash
python3 -m venv myenv
source myenv/bin/activate
```

### Install Dependencies

Install the required Python packages using `pip`:

```bash
pip install -r requirements.txt
```

## Running the Project

### Training Models

You can train models for different tasks using the `train.py` script. Here are the commands to run each task:

#### Classification

```bash
python train.py --task classification
```

#### Regression

```bash
python train.py --task regression
python train.py --task regression 
```

#### Advanced Regression (Exercise 3)

```bash
python train.py --task regression_ex3
```

### Evaluating Models

You can evaluate the trained models using the `eval.py` script. Here are the commands to run each evaluation:

#### Classification

```bash
python eval.py --task classification
```

#### Regression

For regression tasks, specify the target columns used during training:

```bash
python eval.py --task regression --target_columns "Class2.1,Class2.2"
python eval.py --task regression --target_columns "Class7.1,Class7.2,Class7.3"
```

#### Advanced Regression (Exercise 3)

```bash
python eval.py --task regression_ex3
```

## Tasks and Exercises

### Exercise 0: Data Exploration

- **Objective:** Familiarize yourself with the dataset and create diagnostic plots.
- **Steps:**
  - Load the dataset from `data/images` and `data/labels.csv`.
  - Visualize sample images and analyze label distributions.
  - Identify and report any potential issues with the data.

### Exercise 1: Classification

- **Objective:** Classify galaxies into three categories: smooth and round, has a disk, or image is flawed.
- **Steps:**
  - Use columns `[Class1.1, Class1.2, Class1.3]` from `labels.csv`.
  - Convert probabilities into one-hot encoded labels.
  - Develop and train a CNN classification model.
  - Report key performance metrics and document the process.

### Exercise 2: Regression

- **Objective:** Perform regression tasks to answer specific questions about the galaxies.
- **Steps:**
  - Use columns `[Class2.1, Class2.2]` and `[Class7.1, Class7.2, Class7.3]` from `labels.csv`.
  - Develop and train a CNN regression model to predict the floating-point labels.
  - Report key performance metrics and document any constraints.

### Exercise 3: Advanced Regression

- **Objective:** Extend regression tasks to include more detailed questions about the galaxies.
- **Steps:**
  - Use columns `[Class6.1, Class6.2]` and `[Class8.1, Class8.2, Class8.3, Class8.4, Class8.5, Class8.6, Class8.7]`.
  - Enhance the CNN regression model to handle additional outputs.
  - Train the model and report performance metrics.

## Conclusion

This project provides a comprehensive framework for classifying and analyzing galaxy images. Follow the instructions above to set up the project, train models, and evaluate their performance. Happy coding!

If you encounter any issues or have questions, feel free to reach out.

---

**Note:** Ensure you have the dataset downloaded and placed in the appropriate directories (`data/images` and `data/labels.csv`).

**Note:** Due to lack of time, I did not implement argparse to handle command lines involving epochs and batch sizes. All epochs are set to 100 epochs. 

**Note:** Due to lack of time, I was not able to figure out how to create a ROC curve. 

**Note:** The code for eval.py is a bit janky 
```
